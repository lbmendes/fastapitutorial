from typing import List
from typing import Optional

from email_validator import EmailNotValidError
from email_validator import validate_email
from fastapi import Request


class LoginForm:
    def __init__(self, request: Request):
        self.request: Request = request
        self.errors: List = []
        self.username: Optional[str] = None
        self.password: Optional[str] = None

    async def load_data(self):
        form = await self.request.form()
        self.username = form.get("email")
        self.password = form.get("password")

    async def is_valid(self):
        try:
            email = self.username
            validate_email(email)
        except EmailNotValidError as e:
            self.errors.append(str(e))
        if not self.password or not len(self.password) > 4:
            self.errors.append("A valid password is required")
        if not self.errors:
            return True
        return False
