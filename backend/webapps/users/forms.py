from typing import List
from typing import Optional

from db.repository.users import get_user_by_email
from db.repository.users import get_user_by_username
from db.session import get_db
from email_validator import EmailNotValidError
from email_validator import validate_email
from fastapi import Depends
from fastapi import Request
from sqlalchemy.orm import Session


class UserCreateForm:
    def __init__(self, request: Request):
        self.request: Request = request
        self.errors: List = []
        self.username: Optional[str] = None
        self.email: Optional[str] = None
        self.password: Optional[str] = None

    async def load_data(self):
        form = await self.request.form()
        self.username = form.get("username")
        self.email = form.get("email")
        self.password = form.get("password")

    async def is_valid(self, db: Session = Depends(get_db)):

        if not self.username or not len(self.username) > 3:
            self.errors.append("Username should be > 3 chars")
        if get_user_by_username(username=self.username, db=db):
            self.errors.append("There is a user with this username, change it")

        try:
            validate_email(self.email)
        except EmailNotValidError as e:
            self.errors.append(str(e))
        if get_user_by_email(email=self.email, db=db):
            self.errors.append("There is a user with this email, change it")

        if not self.password or not len(self.password) > 4:
            self.errors.append("Password must be > 4 chars")

        if not self.errors:
            return True
        return False
